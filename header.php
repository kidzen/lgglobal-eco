<header>

<?php
//if ($link <> "/Eco/collections.html#collections") {
require("nav.html");
//}
   
?>

    <!--RevSlider-->
    <div class="tp-banner-container">

        <div class="tp-banner" >
            <ul>
                <img style="height: auto" src="img/eco/bk-eco-big.jpg" alt="">
                <!-- SLIDE  -->
                <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                    <!-- MAIN IMAGE -->
                    <img src="img/transparent.png"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption lfl fadeout hidden-xs"
                         data-x="center"
                         data-y="center"
                         data-hoffset="0"
                         data-voffset="0"
                         data-speed="500"
                         data-start="500"
                         data-easing="Power4.easeOut">
                        <img style="width:auto;" src="img/eco/Slides/slidebanner1.png" alt="">
                    </div>

                    <div class="tp-caption lfl fadeout visible-xs"
                         data-x="center"
                         data-y="center"
                         data-hoffset="0"
                         data-voffset="0"
                         data-speed="500"
                         data-start="5"
                         data-easing="Power4.easeOut">
                        <img src="img/eco/Slides/slidebanner1.png" alt="">
                    </div>

                </li>
                <!-- SLIDE 2 -->
                <li data-transition="zoomout" data-slotamount="7" data-masterspeed="1000" >
                    <!-- MAIN IMAGE -->
                    <img src="img/transparent.png"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption lfb fadeout hidden-xs"
                         data-x="center"
                         data-y="center"
                         data-hoffset="0"
                         data-voffset="0"
                         data-speed="500"
                         data-start="500"
                         data-easing="Power4.easeOut">
                        <img style="width:auto;" src="img/eco/Slides/slidebanner2.png" alt="">
                    </div>

                    <div class="tp-caption lfl fadeout visible-xs"
                         data-x="center"
                         data-y="center"
                         data-hoffset="0"
                         data-voffset="0"
                         data-speed="500"
                         data-start="500"
                         data-easing="Power4.easeOut">
                        <img src="img/eco/Slides/slidebanner2.png" alt="">
                    </div>

                </li>

                <!-- SLIDE 3 -->
                <li data-transition="zoomout" data-slotamount="7" data-masterspeed="1000" >
                    <!-- MAIN IMAGE -->
                    <img src="img/transparent.png"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption customin customout hidden-xs"
                         data-x="center"
                         data-y="center"
                         data-hoffset="0"
                         data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-voffset="0"
                         data-speed="500"
                         data-start="500"
                         data-easing="Power4.easeOut">
                        <img style="width:auto;" src="img/eco/Slides/slidebanner3.png" alt="">
                    </div>

                    <div class="tp-caption customin customout visible-xs"
                         data-x="center"
                         data-y="center"
                         data-hoffset="0"
                         data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                         data-voffset="0"
                         data-speed="500"
                         data-start="500"
                         data-easing="Power4.easeOut">
                        <img src="img/eco/Slides/slidebanner3.png" alt="">
                    </div>

                </li>

                <!-- SLIDE 4 -->
                <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                    <!-- MAIN IMAGE -->
                    <img src="img/transparent.png"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption lfl fadeout hidden-xs"
                         data-x="center"
                         data-y="center"
                         data-hoffset="0"
                         data-voffset="0"
                         data-speed="500"
                         data-start="500"
                         data-easing="Power4.easeOut">
                        <img style="width:auto;" src="img/eco/Slides/slidebanner4.png" alt="">
                    </div>

                    <div class="tp-caption lfl fadeout visible-xs"
                         data-x="center"
                         data-y="center"
                         data-hoffset="0"
                         data-voffset="0"
                         data-speed="500"
                         data-start="5"
                         data-easing="Power4.easeOut">
                        <img src="img/eco/Slides/slidebanner4.png" alt="">
                    </div>

                </li>

                <!-- SLIDE 5 -->
                <li data-transition="zoomout" data-slotamount="7" data-masterspeed="1000" >
                    <!-- MAIN IMAGE -->
                    <img src="img/transparent.png"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption lfb fadeout hidden-xs"
                         data-x="center"
                         data-y="center"
                         data-hoffset="0"
                         data-voffset="0"
                         data-speed="500"
                         data-start="500"
                         data-easing="Power4.easeOut">
                        <img style="width:auto;" src="img/eco/Slides/slidebanner5.png" alt="">
                    </div>

                    <div class="tp-caption lfl fadeout visible-xs"
                         data-x="center"
                         data-y="center"
                         data-hoffset="0"
                         data-voffset="0"
                         data-speed="500"
                         data-start="500"
                         data-easing="Power4.easeOut">
                        <img src="img/eco/Slides/slidebanner5.png" alt="">
                    </div>

                </li>

            </ul>
        </div>
    </div>  


</header>