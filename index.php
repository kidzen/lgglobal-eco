<!doctype html>
<!--[if lt IE 7]><html lang="en" class="no-js ie6"><![endif]-->
<!--[if IE 7]><html lang="en" class="no-js ie7"><![endif]-->
<!--[if IE 8]><html lang="en" class="no-js ie8"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->


    <meta charset="UTF-8">
    <title>VTOBath | Eco Shower Head Specialist</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="shortcut icon" href="favicon.png">

    <link rel="stylesheet" href="css/bootstrap.css">

    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="js/rs-plugin/css/settings.css">


    <script type="text/javascript" src="js/modernizr.custom.32033.js"></script>

    <link rel="stylesheet" href="css/eco.css">


    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->



    <body>

        <div class="pre-loader">
            <div class="load-con">
                <img src="img/eco/logo.png" class="animated fadeInDown" alt="VTOlogo">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>
        </div>



        <?php
        require("header.php");

$link = isset($_GET['page'])?$_GET['page']:'home';
//$_
if ($link === "home"){
        require("content.html");
} elseif ($link === "about#whyvto") {
    require("about.html");
}
        require("footer.html");
        ?>
    </div>
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/slick.min.js"></script>
    <script src="js/placeholdem.min.js"></script>
    <script src="js/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script src="js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/scripts.js"></script>
    <script>
        $(document).ready(function () {
            appMaster.preLoader();
        });
        $(function () {
            $('#homeCarousel').carousel({
                interval: 3000,
                pause: "false"
            });
        });
    </script>

</body>

</html>
