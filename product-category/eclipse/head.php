<!DOCTYPE html>
<html lang="en-US" class="no-js" >

    <!-- Mirrored from kallyas.net/demo/product-category/clothing/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 09 Oct 2015 14:33:18 GMT -->
    <!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
    <head>
        <meta charset="UTF-8"/>
        <meta name="twitter:widgets:csp" content="on">
        <link rel="profile" href="http://gmpg.org/xfn/11"/>
        <link rel="pingback" href="../../xmlrpc.php"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <title>ECLIPSE SERIES</title>
        <link rel="alternate" type="application/rss+xml" title="Kallyas Demo &raquo; Feed" href="../../feed/index.html" />
        <link rel="alternate" type="application/rss+xml" title="Kallyas Demo &raquo; Comments Feed" href="../../comments/feed/index.html" />
        <script type="text/javascript">
            window._wpemojiSettings = {"baseUrl":"http:\/\/s.w.org\/images\/core\/emoji\/72x72\/", "ext":".png", "source":{"concatemoji":"http:\/\/kallyas.net\/demo\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.3.1"}};
                    !function(a, b, c){function d(a){var c = b.createElement("canvas"), d = c.getContext && c.getContext("2d"); return d && d.fillText?(d.textBaseline = "top", d.font = "600 32px Arial", "flag" === a?(d.fillText(String.fromCharCode(55356, 56812, 55356, 56807), 0, 0), c.toDataURL().length > 3e3):(d.fillText(String.fromCharCode(55357, 56835), 0, 0), 0 !== d.getImageData(16, 16, 1, 1).data[0])):!1}function e(a){var c = b.createElement("script"); c.src = a, c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)}var f, g; c.supports = {simple:d("simple"), flag:d("flag")}, c.DOMReady = !1, c.readyCallback = function(){c.DOMReady = !0}, c.supports.simple && c.supports.flag || (g = function(){c.readyCallback()}, b.addEventListener?(b.addEventListener("DOMContentLoaded", g, !1), a.addEventListener("load", g, !1)):(a.attachEvent("onload", g), b.attachEvent("onreadystatechange", function(){"complete" === b.readyState && c.readyCallback()})), f = c.source || {}, f.concatemoji?e(f.concatemoji):f.wpemoji && f.twemoji && (e(f.twemoji), e(f.wpemoji)))}(window, document, window._wpemojiSettings);</script>
        <style type="text/css">
            img.wp-smiley,
            img.emoji {
                display: inline !important;
                border: none !important;
                box-shadow: none !important;
                height: 1em !important;
                width: 1em !important;
                margin: 0 .07em !important;
                vertical-align: -0.1em !important;
                background: none !important;
                padding: 0 !important;
            }
        </style>
        <link rel='stylesheet' id='zn_all_g_fonts-css'  href='http://fonts.googleapis.com/css?family=Montserrat%3Aregular%2C500%2C700%7COpen+Sans%3Aregular%2C300%2C600%2C700%2C800&amp;ver=4.3.1' type='text/css' media='all' />
        <link rel='stylesheet' id='cuteslider-css'  href='../../kallyaswp.hogashstudio.netdna-cdn.com/demo/wp-content/plugins/CuteSlider/css/cutesliderc64e.css?ver=1.1.1' type='text/css' media='all' />
        <link rel='stylesheet' id='rs-plugin-settings-css'  href='../../kallyaswp.hogashstudio.netdna-cdn.com/demo/wp-content/plugins/revslider/public/assets/css/settingsaaf9.css?ver=5.0.8' type='text/css' media='all' />
        <style id='rs-plugin-settings-inline-css' type='text/css'>
            .tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
        </style>
        <link rel='stylesheet' id='woocommerce-layout-css'  href='../../wp-content/plugins/woocommerce/assets/css/woocommerce-layout2f91.css?ver=2.4.6' type='text/css' media='all' />
        <link rel='stylesheet' id='woocommerce-smallscreen-css'  href='../../wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen2f91.css?ver=2.4.6' type='text/css' media='only screen and (max-width: 768px)' />
        <link rel='stylesheet' id='woocommerce-general-css'  href='../../wp-content/plugins/woocommerce/assets/css/woocommerce2f91.css?ver=2.4.6' type='text/css' media='all' />
        <link rel='stylesheet' id='demo_panel-css'  href='../../kallyaswp.hogashstudio.netdna-cdn.com/demo/wp-content/themes/kallyas-child/_demo-panel/dp-styles05da.css?ver=4.0.2' type='text/css' media='all' />
        <link rel='stylesheet' id='th-bootstrap-styles-css'  href='../../kallyaswp.hogashstudio.netdna-cdn.com/demo/wp-content/themes/kallyas/css/bootstrap.min05da.css?ver=4.0.2' type='text/css' media='all' />
        <link rel='stylesheet' id='kallyas-styles-css'  href='../../kallyaswp.hogashstudio.netdna-cdn.com/demo/wp-content/themes/kallyas-child/style05da.css?ver=4.0.2' type='text/css' media='all' />
        <link rel='stylesheet' id='th-theme-template-styles-css'  href='../../kallyaswp.hogashstudio.netdna-cdn.com/demo/wp-content/themes/kallyas/css/template05da.css?ver=4.0.2' type='text/css' media='all' />
        <link rel='stylesheet' id='woocommerce-overrides-css'  href='../../kallyaswp.hogashstudio.netdna-cdn.com/demo/wp-content/themes/kallyas/css/kl-woocommerce05da.css?ver=4.0.2' type='text/css' media='all' />
        <link rel='stylesheet' id='th-bs-responsive-styles-css'  href='../../kallyaswp.hogashstudio.netdna-cdn.com/demo/wp-content/themes/kallyas/css/bootstrap-responsive05da.css?ver=4.0.2' type='text/css' media='all' />
        <link rel='stylesheet' id='th-theme-options-styles-css'  href='../../kallyaswp.hogashstudio.netdna-cdn.com/demo/wp-content/uploads/zn_dynamic05da.css?ver=4.0.2' type='text/css' media='all' />
        <link rel='stylesheet' id='zn_pb_css-css'  href='../../kallyaswp.hogashstudio.netdna-cdn.com/demo/wp-content/uploads/zn_pb_css5b31.css?ver=4.3.1' type='text/css' media='all' />
        <script type='text/javascript'>
                    /* <![CDATA[ */
                    var CSSettings = {"pluginPath":"http:\/\/kallyas.net\/demo\/wp-content\/plugins\/CuteSlider"};
                    /* ]]> */
        </script>
        <script type='text/javascript' src='../../kallyaswp.hogashstudio.netdna-cdn.com/demo/wp-content/plugins/CuteSlider/js/cute.sliderc64e.js?ver=1.1.1'></script>
        <script type='text/javascript' src='../../kallyaswp.hogashstudio.netdna-cdn.com/demo/wp-content/plugins/CuteSlider/js/cute.transitions.allc64e.js?ver=1.1.1'></script>
        <script type='text/javascript' src='../../../../cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.minf488.js?ver=1.1.0'></script>
        <script type='text/javascript' src='../../kallyaswp.hogashstudio.netdna-cdn.com/demo/wp-includes/js/jquery/jqueryc1d8.js?ver=1.11.3'></script>
        <script type='text/javascript' src='../../kallyaswp.hogashstudio.netdna-cdn.com/demo/wp-includes/js/jquery/jquery-migrate.min1576.js?ver=1.2.1'></script>
        <script type='text/javascript' src='../../kallyaswp.hogashstudio.netdna-cdn.com/demo/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.minaaf9.js?ver=5.0.8'></script>
        <script type='text/javascript' src='../../kallyaswp.hogashstudio.netdna-cdn.com/demo/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.minaaf9.js?ver=5.0.8'></script>
        <script type='text/javascript' src='../../kallyaswp.hogashstudio.netdna-cdn.com/demo/wp-content/themes/kallyas-child/_demo-panel/dp-scripts05da.js?ver=4.0.2'></script>
        <script type='text/javascript' src='../../kallyaswp.hogashstudio.netdna-cdn.com/demo/wp-content/themes/kallyas/js/bootstrap.min05da.js?ver=4.0.2'></script>
        <script type='text/javascript' src='../../kallyaswp.hogashstudio.netdna-cdn.com/demo/wp-content/themes/kallyas/js/modernizr05da.js?ver=4.0.2'></script>
        <link rel="EditURI" type="application/rsd+xml" title="RSD" href="../../xmlrpc0db0.php?rsd" />
        <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://kallyaswp.hogashstudio.netdna-cdn.com/demo/wp-includes/wlwmanifest.xml" /> 
        <meta name="generator" content="WordPress 4.3.1" />
        <meta name="generator" content="WooCommerce 2.4.6" />
        <link rel="alternate" type="application/rss+xml"  title="New products added to CLOTHING" href="../../shop/feed/index623d.html?product_cat=clothing" /> 	<!-- GA Tracking Code -->
        <script>
                    (function(i, s, o, g, r, a, m){i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function(){
                    (i[r].q = i[r].q || []).push(arguments)}, i[r].l = 1 * new Date(); a = s.createElement(o),
                            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
                    })(window, document, 'script', '../../../../www.google-analytics.com/analytics.js', 'ga');
                    ga('create', 'UA-62562526-3', 'auto');
                    ga('send', 'pageview');</script>
        

        <link rel="shortcut icon" href="../../kallyaswp.hogashstudio.netdna-cdn.com/demo/wp-content/uploads/2015/08/vtoicon.png"/>        <!--[if lte IE 9]>
                <link rel="stylesheet" type="text/css" href="http://kallyaswp.hogashstudio.netdna-cdn.com/demo/wp-content/themes/kallyas/css/fixes.css"/>
                <![endif]-->
        <!--[if lte IE 8]>
        <script src="http://kallyaswp.hogashstudio.netdna-cdn.com/demo/wp-content/themes/kallyas/js/respond.js"></script>
        <script type="text/javascript">
            var $buoop = {
                vs: { i: 8, f: 6, o: 10.6, s: 4, n: 9 }
            };

            $buoop.ol = window.onload;

            window.onload = function(){
                try {
                    if ($buoop.ol) {
                        $buoop.ol()
                    }
                }
                catch (e) {}

                var e = document.createElement("script");
                e.setAttribute("type", "text/javascript");
                e.setAttribute("src", "http://browser-update.org/update.js");
                document.body.appendChild(e);
            };
        </script>
        <![endif]-->

        <!-- for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
        <meta name="generator" content="Powered by Slider Revolution 5.0.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
        <style type="text/css" id="wpk_local_adminbar_notice_styles">
            #wpadminbar .ab-top-menu .wpk-kallyas-options-menu-item:hover div,
            #wpadminbar .ab-top-menu .wpk-kallyas-options-menu-item:active div,
            #wpadminbar .ab-top-menu .wpk-kallyas-options-menu-item:focus div,
            #wpadminbar .ab-top-menu .wpk-kallyas-options-menu-item div {
                color: #eee;
                cursor: default;
                background: #222;
                position: relative;
            }
            #wpadminbar .ab-top-menu .wpk-kallyas-options-menu-item:hover div {
                color: #45bbe6 !important;
            }
            #wpadminbar .ab-top-menu .wpk-kallyas-options-menu-item > .ab-item:before {
                content: '\f111';
                top: 2px;
            }
        </style>
        <link rel="shortcut icon" href="../../favicon.png">

        <link rel="stylesheet" href="../../css/bootstrap.css">

        <link rel="stylesheet" href="../../css/animate.css">
        <link rel="stylesheet" href="../../css/font-awesome.min.css">
        <link rel="stylesheet" href="css/slick.css">
        <link rel="stylesheet" href="../../js/rs-plugin/css/settings.css">


        <script type="text/javascript" src="../../js/modernizr.custom.32033.js"></script>

        <link rel="stylesheet" href="../../css/eco.css">